# ci-deploy-script
I use this deploy script for GitLab CI to build static website templates and also to generate archives of my own Wordpress plugins and themes.

## Assumptions
* You don't want your dependencies/distribution files inside the repo
* Using npm and optionally Composer for dependencies
* Using Gulp for building the project
* (Optional) Using icons from Font Awesome which require a Pro license
* Branch `master` is production ready
* A job `npm run deploy` which builds the project ready for deployment (resulting in either a static website containing multiple files, or a single ZIP archive)
* `npm run deploy` builds into `dist/`folder
* You want to deploy to a FTP server

## Setup
1. Setup your package managers, Gulp script, etc.
2. Add `/dist` to `.gitignore`
3. Copy [.gitlab-ci.yml](.gitlab-ci.yml) into the root of your project
4. Set these CI variables in GitLab project settings:
   * `HOST`: FTP server address
   * `USERNAME`: FTP username
   * `PASSWORD`: FTP password
   * `FA_TOKEN` (optional): Your Font Awesome license key
